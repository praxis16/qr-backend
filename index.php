<?php
    include('phpqrcode/qrlib.php');
    include('ApiMiddleware.php');
    ini_set('max_execution_time', 600);
    function generateRandomQR($app, $api) {
        $qr = md5(bin2hex(openssl_random_pseudo_bytes(20)));
        /*$insert_query = "INSERT INTO `qrid`(`qr_id`, `app`) VALUES (\"$qr\", \"$app\")";
        if(mysqli_query($conn, $insert_query)) {
            return $qr;
        } else {
            return generateRandomQR($conn);
        }*/
        $request = array(
            'url'    => 'qrid',
            'method' => 'POST',
            'body'   => array('qr_id' => $qr, 'app' => $app)
        );
        $response = $api->initRequest($request);
        if($response['http_code'] == 200 || $response['http_code'] == 201) {
            return $response['data'][0]['qr_id'];
        } else {
            return generateRandomQR($app, $api);
        }
    }

    function saveAsPNG($qrContent, $withLogo, $dirname, $filename) {
        $tempImg = "temp/temp.png";
        QRcode::png($qrContent, $tempImg, QR_ECLEVEL_H, 10);
        $QR = imagecreatefrompng($tempImg);
        if($withLogo !== FALSE){
            $logo = imagecreatefromstring(file_get_contents("logo.jpeg"));
            $QR_width = imagesx($QR);
            $QR_height = imagesy($QR);

            $logo_width = imagesx($logo);
            $logo_height = imagesy($logo);

            // Scale logo to fit in the QR Code
            $logo_qr_width = $QR_width/5;
            $scale = $logo_width/$logo_qr_width;
            $logo_qr_height = $logo_height/$scale;

            imagecopyresampled($QR, $logo, 2*$QR_width/5, 2*$QR_height/5, 0, 0, $logo_qr_width, $logo_qr_height, $logo_width, $logo_height);
        }
        imagepng($QR, $dirname.DIRECTORY_SEPARATOR.$filename.'.png', 0);
        imagedestroy($QR);
    }
    $success = "";
    if($_SERVER["REQUEST_METHOD"] == "POST") {
        $api = new ApiMiddleware();
        $count = $_POST['count'];
        $withLogo = isset($_POST['with-logo']);
        $app = isset($_POST['app']) ? 1 : 0;
        date_default_timezone_set("Asia/Kolkata");
        $dirname = date("m-d-Y H-i-s");
        mkdir($dirname);
        for($i = 0; $i < $count; $i++) {
            $qrContent = generateRandomQR($app, $api);
            if($app === 0)
              saveAsPNG($qrContent, $withLogo, $dirname, $i + 1);
        }
        $success = "QRs created successfully and output to $dirname";
    }

    include('templates/index-form.html');
