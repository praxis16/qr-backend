<?php
    include('db_connection.php');
    header('Content-type: image/png');
    if(isset($_GET['data'])) {
        include('phpqrcode/qrlib.php');
        $codeContents = md5($_GET['data']);
        $tempImg = "temp/temp.png";
        QRcode::png($codeContents, $tempImg, QR_ECLEVEL_H);
        imagepng(imagecreatefrompng($tempImg));
    }
