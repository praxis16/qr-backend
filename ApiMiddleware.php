<?php

    /**
     * Class ApiMiddleware
     * Include this file wherever required. Do not forget to instantiate the class.
     * Username and password are not to be disclosed.
     * This class is used to interact with the RESTful api as a middleware that makes the actual request to get required
     * data from database.
     * @usage call function initRequest with the apt. parameters. The response is returned by this function.
     */
    class ApiMiddleware {
        public static $BASE_URL = "http://localhost/praxis-api/index.php/";
        private $username, $password;

        public function __construct() {
             $this->username = "api_user";
             $this->password = "praxis2k16RESTapi";
        }

        /**
         * @param $request ArrayObject object holding the body of request to be made
         * @return mixed $rawResponse response data as Assoc array
         * @throws Exception
         * @example
         *  $request = array(
         *              'url'    => 'some-controller/some-function/some-arg',
         *              'method' => 'GET|POST|PUT|DELETE',
         *              'body'   => NULL | [<ArrayObject of request body>]
         *              )
         */
        public function initRequest($request) {
            $options = $this->cURLAuthDigest($request['url']);
            switch($request['method']) {
                case 'GET':
                    break;
                case 'POST':
                    $options[CURLOPT_POST] = TRUE;
                    $options[CURLOPT_POSTFIELDS] = json_encode($request['body']);
                    break;
                case 'PUT':
                    $options[CURLOPT_CUSTOMREQUEST] = "PUT";
                    $options[CURLOPT_POSTFIELDS] = json_encode($request['body']);
                    break;
                case 'DELETE':
                    $options[CURLOPT_CUSTOMREQUEST] = "DELETE";
                    break;
            }
            return $this->execRequest($options);
        }

        /**
         * @param $options
         * @return mixed
         * @throws Exception
         */
        private function execRequest($options) {
            print_r($options);
            $ch = curl_init();

            curl_setopt_array( $ch, $options );

            try {
                $raw_response  = curl_exec( $ch );

                // validate CURL status
                if(curl_errno($ch))
                    throw new Exception(curl_error($ch), 500);

                /*// validate HTTP status code (user/password credential issues)
                $status_code = curl_getinfo($ch, CURLINFO_HTTP_CODE);
                if ($status_code != 200)
                    throw new Exception("Response with Status Code [" . $status_code . "].", 500);*/

            } catch(Exception $ex) {
                if ($ch != null) curl_close($ch);
                throw new Exception($ex);
            }
            $response = curl_getinfo($ch);
            if ($ch != null) {
                curl_close($ch);
            }

            $response['data'] = json_decode($raw_response, true);
            return $response;
        }

        /**
         * @param $url mixed url of request
         * @return array options for cURL request
         */
        private function cURLAuthDigest($url) {
            $headers = array();
            $headers[] = 'Content-Type: application/json';
            $options = array(
                CURLOPT_URL            => ApiMiddleware::$BASE_URL . $url,
                CURLOPT_HEADER         => false,
                CURLOPT_VERBOSE        => true,
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_FOLLOWLOCATION => true,
                CURLOPT_SSL_VERIFYPEER => false,    // for https
                CURLOPT_USERPWD        => $this->username . ":" . $this->password,
                CURLOPT_HTTPAUTH       => CURLAUTH_DIGEST,
                CURLOPT_HTTPHEADER     => $headers
            );
            return $options;
        }
    }